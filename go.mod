module gitlab.com/ilia_reist/client-test

go 1.16

require (
	github.com/google/uuid v1.3.0
	github.com/stretchr/testify v1.7.0
)
