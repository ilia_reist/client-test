//  e2e

package main

import (
	"encoding/json"
	"fmt"
	"os"
	"time"

	"github.com/google/uuid"
	Form3 "gitlab.com/ilia_reist/client-test/form3"
	Models "gitlab.com/ilia_reist/client-test/form3/models"
)

func main() {
	api := os.Getenv("FORM3_API")

	f3 := Form3.New(api, 1*time.Second)

	version := int64(0)
	classification := "Personal"
	status := "confirmed"
	country := "GB"
	id := uuid.NewString()
	acct := Models.AccountData{
		ID:             id,
		OrganisationID: "625df21a-fa06-4b66-b344-371e08fc3ebe",
		Type:           "accounts",
		Version:        &version,
		Attributes: &Models.AccountAttributes{
			AccountClassification:   &classification,
			AccountMatchingOptOut:   new(bool),
			AccountNumber:           "12345678",
			AlternativeNames:        []string{"the Boss"},
			BankID:                  "123456",
			BankIDCode:              "GBDSC",
			BaseCurrency:            "GBP",
			Bic:                     "AAABCDDD",
			Country:                 &country,
			Iban:                    "AL472121100900000002356987411",
			JointAccount:            new(bool),
			Name:                    []string{"the Boss"},
			SecondaryIdentification: "MYID123",
			Status:                  &status,
			Switched:                new(bool),
		},
	}

	result, _ := f3.Account.Create(acct)
	resJson, err := json.Marshal(result)
	fmt.Println(string(resJson), err)
}
