package form3

import (
	Account "gitlab.com/ilia_reist/client-test/form3/account"
	Model "gitlab.com/ilia_reist/client-test/form3/models"
	"time"
)

type Form3 struct {
	Account clientInterface
}

type clientInterface interface {
	Create(account Model.AccountData) (Model.AccountData, error)
	Delete(accountID string, version int) (bool, error)
	Fetch(accountID string) (Model.AccountData, error)
}

func New(host string, timeout time.Duration) *Form3 {
	return &Form3{
		Account: Account.NewClient(host, timeout),
	}
}
