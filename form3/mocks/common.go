package mocks

import (
	"bytes"
	"io"
	"io/ioutil"
	"log"
	"net/http"
)

type RoundTripFunc func(req *http.Request) *http.Response

func (f RoundTripFunc) RoundTrip(req *http.Request) (*http.Response, error) {
	return f(req), nil
}

func TestClient(code int, body []byte) *http.Client {
	return &http.Client{
		Transport: RoundTripFunc(func(request *http.Request) *http.Response {
			return &http.Response{
				StatusCode: code,
				Body:       io.NopCloser(bytes.NewReader(body)),
			}
		}),
	}
}

func ReadFile(filePath string) []byte {
	content, err := ioutil.ReadFile(filePath)
	if err != nil {
		log.Panicf("Cannot open test response file '%s'", filePath)
	}
	return content
}
