package account

import (
	"fmt"
	"net/http"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	Mocks "gitlab.com/ilia_reist/client-test/form3/mocks"
	Models "gitlab.com/ilia_reist/client-test/form3/models"
)

func NewAccount() Models.AccountData {
	version := int64(0)
	classification := "Personal"
	status := "confirmed"
	country := "GB"
	return Models.AccountData{
		ID:             "1e719fc3-14a1-47eb-8b29-1bfcf6e6dd5f",
		OrganisationID: "625df21a-fa06-4b66-b344-371e08fc3ebe",
		Type:           "accounts",
		Version:        &version,
		Attributes: &Models.AccountAttributes{
			AccountClassification:   &classification,
			AccountMatchingOptOut:   new(bool),
			AccountNumber:           "12345678",
			AlternativeNames:        []string{"the Boss"},
			BankID:                  "123456",
			BankIDCode:              "GBDSC",
			BaseCurrency:            "GBP",
			Bic:                     "AAABCDDD",
			Country:                 &country,
			Iban:                    "AL472121100900000002356987411",
			JointAccount:            new(bool),
			Name:                    []string{"the Boss"},
			SecondaryIdentification: "MYID123",
			Status:                  &status,
			Switched:                new(bool),
		},
	}
}

func TestClientCreateEmptyAccount(t *testing.T) {
	// Arrange
	client := NewClient("http://0.0.0.0:8080", 5*time.Second)

	// Act
	res, err := client.Create(Models.AccountData{})
	fmt.Println(res)
	// Assert
	assert.NotNil(t, err)
}

func TestClientCreateValidAccount(t *testing.T) {
	// Arrange
	mockResponse := Mocks.ReadFile(MOCK_RESPONSE_PATH)

	client := Client{
		Host:   "http://0.0.0.0:8080",
		Client: Mocks.TestClient(201, mockResponse),
	}

	// Act
	acct := NewAccount()
	account, err := client.Create(acct)

	// Assert
	assert.Nil(t, err)
	assert.Equal(t, "accounts", account.Type)
	assert.Equal(t, int64(0), *account.Version)
	assert.Equal(t, "GB", *account.Attributes.Country)
}

func TestClientCreateWrongAddress(t *testing.T) {
	// Arrange
	client := Client{
		Host:   "http://bad.url",
		Client: &http.Client{Timeout: 1 * time.Second},
	}

	// Act
	acct := NewAccount()
	_, err := client.Create(acct)
	assert.Nil(t, err)
}
