package account

import (
	"net/http"
	"time"
)

const (
	APIVERSION string = "v1"
	PATH       string = "organisation/accounts"
)

type Client struct {
	Host   string
	Client *http.Client
}

func NewClient(host string, timeout time.Duration) *Client {
	return &Client{
		Host:   host,
		Client: &http.Client{Timeout: timeout},
	}
}
