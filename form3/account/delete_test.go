package account

import (
	"github.com/stretchr/testify/assert"
	Mocks "gitlab.com/ilia_reist/client-test/form3/mocks"

	"net/http"
	"testing"
	"time"
)

const (
	MOCK_RESPONSE_PATH = "../mocks/mock_responses/create_acc_response.json"
)

func TestDeleteNonExistentAccount(t *testing.T) {
	// Arrange
	mockResponse := Mocks.ReadFile(MOCK_RESPONSE_PATH)
	client := &Client{
		Host:   "http://0.0.0.0:8080",
		Client: Mocks.TestClient(404, mockResponse),
	}

	// Act
	_, err := client.Delete("non-existent-id", 0)

	// Assert
	assert.NotNil(t, err)
	assert.Equal(t, err.Error(), "cannot find account ID non-existent-id")
}

func TestDeleteEmptyAccount(t *testing.T) {
	// Arrange
	mockResponse := Mocks.ReadFile(MOCK_RESPONSE_PATH)
	client := &Client{
		Host:   "http://0.0.0.0:8080",
		Client: Mocks.TestClient(204, mockResponse),
	}

	// Act
	_, err := client.Delete("", 0)

	// Assert
	assert.NotNil(t, err)
}

func TestClientDeleteNegativeVersion(t *testing.T) {
	// Arrange
	mockResponse := Mocks.ReadFile(MOCK_RESPONSE_PATH)
	client := &Client{
		Host:   "http://0.0.0.0:8080",
		Client: Mocks.TestClient(204, mockResponse),
	}

	// Act
	_, err := client.Delete("existing-id", -1)

	// Assert
	assert.NotNil(t, err)
}

func TestDeleteWrongVersion(t *testing.T) {
	// Arrange
	mockResponse := Mocks.ReadFile(MOCK_RESPONSE_PATH)
	client := &Client{
		Host:   "http://0.0.0.0:8080",
		Client: Mocks.TestClient(409, mockResponse),
	}

	// Act
	_, err := client.Delete("id", 1)

	// Assert
	assert.NotNil(t, err)
}

func TestClientDeleteValidAccount(t *testing.T) {
	// Arrange
	mockResponse := Mocks.ReadFile(MOCK_RESPONSE_PATH)
	client := &Client{
		Host:   "http://0.0.0.0:8080",
		Client: Mocks.TestClient(204, mockResponse),
	}

	// Act
	result, err := client.Delete("id", 0)

	// Assert
	assert.Nil(t, err)
	assert.True(t, result)
}

func TestClientDeleteWrongAddress(t *testing.T) {
	// Arrange
	client := &Client{
		Host:   "http://wrong.url",
		Client: &http.Client{Timeout: 1 * time.Second},
	}

	// Act
	_, err := client.Delete("id", 1)

	// Assert
	assert.NotNil(t, err)
}
