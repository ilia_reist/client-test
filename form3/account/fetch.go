package account

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"

	Model "gitlab.com/ilia_reist/client-test/form3/models"
)

func (c *Client) Fetch(accountID string) (Model.AccountData, error) {
	var request *http.Request
	var response *http.Response
	var err error

	if accountID == "" {
		return Model.AccountData{}, errors.New("cannot process empty accountID")
	}

	uri := fmt.Sprintf("%s/%s/%s/%s", c.Host, APIVERSION, PATH, accountID)
	request, _ = http.NewRequest("GET", uri, nil)

	response, err = c.Client.Do(request)
	if err != nil {
		return Model.AccountData{}, err
	}

	if response.StatusCode != http.StatusOK {
		errorMessage := fmt.Sprintf("wrong code %d", response.StatusCode)
		return Model.AccountData{}, errors.New(errorMessage)
	}
	defer response.Body.Close()

	responseContent, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return Model.AccountData{}, err
	}

	var serverResponse struct {
		Data Model.AccountData `json:"data"`
	}
	err = json.Unmarshal(responseContent, &serverResponse)
	if err != nil {
		return Model.AccountData{}, err
	}

	return serverResponse.Data, nil
}
