package e2e_test

import (
	"os"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	Form3 "gitlab.com/ilia_reist/client-test/form3"
	Models "gitlab.com/ilia_reist/client-test/form3/models"
)

func TestCreateUserE2E(t *testing.T) {
	api := os.Getenv("FORM3_API")

	if api == "" {
		t.Skip("Please specify FORM3_API address")
	}
	f3 := Form3.New(api, 1*time.Second)

	version := int64(0)
	classification := "Personal"
	status := "confirmed"
	country := "GB"
	id := uuid.NewString()
	acct := Models.AccountData{
		ID:             id,
		OrganisationID: "625df21a-fa06-4b66-b344-371e08fc3ebe",
		Type:           "accounts",
		Version:        &version,
		Attributes: &Models.AccountAttributes{
			AccountClassification:   &classification,
			AccountMatchingOptOut:   new(bool),
			AccountNumber:           "12345678",
			AlternativeNames:        []string{"the Boss"},
			BankID:                  "123456",
			BankIDCode:              "GBDSC",
			BaseCurrency:            "GBP",
			Bic:                     "AAABCDDD",
			Country:                 &country,
			Iban:                    "AL472121100900000002356987411",
			JointAccount:            new(bool),
			Name:                    []string{"the Boss"},
			SecondaryIdentification: "MYID123",
			Status:                  &status,
			Switched:                new(bool),
		},
	}

	result, err := f3.Account.Create(acct)

	assert.Nil(t, err)
	assert.Equal(t, id, result.ID)
}

func TestFetchUserE2E(t *testing.T) {
	api := os.Getenv("FORM3_API")
	if api == "" {
		t.Skip("Please specify FORM3_API address")
	}
	f3 := Form3.New(api, 1*time.Second)

	version := int64(0)
	classification := "Personal"
	status := "confirmed"
	country := "GB"
	id := uuid.NewString()
	acct := Models.AccountData{
		ID:             id,
		OrganisationID: "625df21a-fa06-4b66-b344-371e08fc3ebe",
		Type:           "accounts",
		Version:        &version,
		Attributes: &Models.AccountAttributes{
			AccountClassification:   &classification,
			AccountMatchingOptOut:   new(bool),
			AccountNumber:           "12345678",
			AlternativeNames:        []string{"the Boss"},
			BankID:                  "123456",
			BankIDCode:              "GBDSC",
			BaseCurrency:            "GBP",
			Bic:                     "AAABCDDD",
			Country:                 &country,
			Iban:                    "AL472121100900000002356987411",
			JointAccount:            new(bool),
			Name:                    []string{"the Boss"},
			SecondaryIdentification: "MYID123",
			Status:                  &status,
			Switched:                new(bool),
		},
	}

	f3.Account.Create(acct)
	time.Sleep(1 * time.Second)
	result, err := f3.Account.Fetch(id)

	assert.Nil(t, err)
	assert.Equal(t, id, result.ID)
}

func TestDeleteUserE2E(t *testing.T) {
	api := os.Getenv("FORM3_API")
	if api == "" {
		t.Skip("Cannot test due to empty FORM3_API")
	}
	f3 := Form3.New(api, 1*time.Second)

	version := int64(0)
	classification := "Personal"
	status := "confirmed"
	country := "GB"
	id := uuid.NewString()
	acct := Models.AccountData{
		ID:             id,
		OrganisationID: "625df21a-fa06-4b66-b344-371e08fc3ebe",
		Type:           "accounts",
		Version:        &version,
		Attributes: &Models.AccountAttributes{
			AccountClassification:   &classification,
			AccountMatchingOptOut:   new(bool),
			AccountNumber:           "12345678",
			AlternativeNames:        []string{"the Boss"},
			BankID:                  "123456",
			BankIDCode:              "GBDSC",
			BaseCurrency:            "GBP",
			Bic:                     "AAABCDDD",
			Country:                 &country,
			Iban:                    "AL472121100900000002356987411",
			JointAccount:            new(bool),
			Name:                    []string{"the Boss"},
			SecondaryIdentification: "MYID123",
			Status:                  &status,
			Switched:                new(bool),
		},
	}

	f3.Account.Create(acct)
	time.Sleep(1 * time.Second)
	result, err := f3.Account.Delete(id, 0)

	assert.Nil(t, err)
	assert.True(t, result)
}
