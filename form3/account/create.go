package account

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"

	Model "gitlab.com/ilia_reist/client-test/form3/models"
)

func (c *Client) Create(account Model.AccountData) (Model.AccountData, error) {
	var request *http.Request
	var response *http.Response
	var payload []byte
	var err error

	type Account struct {
		Data Model.AccountData `json:"data"`
	}
	data := &Account{Data: account}

	payload, err = json.Marshal(data)
	if err != nil {
		return Model.AccountData{}, err
	}

	uri := fmt.Sprintf("%s/%s/%s", c.Host, APIVERSION, PATH)
	request, _ = http.NewRequest("POST", uri, bytes.NewBuffer(payload))

	response, err = c.Client.Do(request)
	if err != nil {
		return Model.AccountData{}, err
	}

	if response.StatusCode != http.StatusCreated {
		errorMessage := fmt.Sprintf("wrong code %d", response.StatusCode)

		return Model.AccountData{}, errors.New(errorMessage)
	}
	defer response.Body.Close()

	responseContent, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return Model.AccountData{}, err
	}

	var serverResponse struct {
		Data Model.AccountData `json:"data"`
	}
	err = json.Unmarshal(responseContent, &serverResponse)
	if err != nil {
		return Model.AccountData{}, err
	}

	return serverResponse.Data, nil
}
