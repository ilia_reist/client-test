package account

import (
	"net/http"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	Mocks "gitlab.com/ilia_reist/client-test/form3/mocks"
)

func TestFetchNonExistentAccount(t *testing.T) {
	// Arrange
	mockResponse := Mocks.ReadFile(MOCK_RESPONSE_PATH)
	client := &Client{
		Host:   "http://0.0.0.0:8080",
		Client: Mocks.TestClient(404, mockResponse),
	}

	// Act
	_, err := client.Fetch("non-existent-id")

	// Assert
	assert.NotNil(t, err)
	assert.Equal(t, err.Error(), "wrong code 404")
}

func TestFetchEmptyAccount(t *testing.T) {
	// Arrange
	mockResponse := Mocks.ReadFile(MOCK_RESPONSE_PATH)
	client := &Client{
		Host:   "http://0.0.0.0:8080",
		Client: Mocks.TestClient(404, mockResponse),
	}

	// Act
	_, err := client.Fetch("")

	// Assert
	assert.NotNil(t, err)
}

func TestFetchValidAccount(t *testing.T) {
	// Arrange
	mockResponse := Mocks.ReadFile(MOCK_RESPONSE_PATH)
	client := &Client{
		Host:   "http://0.0.0.0:8080",
		Client: Mocks.TestClient(200, mockResponse),
	}

	// Act
	result, err := client.Fetch("8fcd3015-b774-4244-b4e2-12c15cc8af90")

	// Assert
	assert.Nil(t, err)
	assert.Equal(t, "8fcd3015-b774-4244-b4e2-12c15cc8af90", result.ID)
}

func TestFetchWrongServer(t *testing.T) {
	// Arrange
	client := &Client{
		Host:   "http://bad.url",
		Client: &http.Client{Timeout: 1 * time.Second},
	}

	// Act
	_, err := client.Fetch("existing-id")

	// Assert
	assert.NotNil(t, err)
}
