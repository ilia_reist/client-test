package account

import (
	"errors"
	"fmt"
	"net/http"
)

func (c *Client) Delete(accountID string, version int) (bool, error) {
	var request *http.Request
	var response *http.Response
	var errorMessage string
	var err error

	if accountID == "" {
		return false, errors.New("cannot process empty accountID")
	}
	if version < 0 {
		return false, errors.New("cannot process negative version")
	}

	uri := fmt.Sprintf("%s/%s/%s/%s?version=%d", c.Host, APIVERSION, PATH, accountID, version)
	request, _ = http.NewRequest("DELETE", uri, nil)

	response, err = c.Client.Do(request)
	if err != nil {
		return false, err
	}
	defer response.Body.Close()

	switch response.StatusCode {
	case http.StatusNoContent:
		return true, nil
	case http.StatusNotFound:
		errorMessage = fmt.Sprintf("cannot find account ID %s", accountID)
	case http.StatusConflict:
		errorMessage = fmt.Sprintf("cannot process version %d for account %s", version, accountID)
	default:
		errorMessage = fmt.Sprintf("unknown status code %d", response.StatusCode)
	}

	return false, errors.New(errorMessage)
}
