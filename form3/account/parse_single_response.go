package account

import (
	"encoding/json"
	"gitlab.com/ilia_reist/client-test/form3/models"

	"io/ioutil"
	"net/http"
)

func ParseSingleResponse(response *http.Response) (models.AccountData, error) {
	defer response.Body.Close()
	content, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return models.AccountData{}, err
	}

	var serverResponse struct {
		Data  models.AccountData `json:"data"`
		Links models.Links       `json:"links"`
	}

	err = json.Unmarshal(content, &serverResponse)
	if err != nil {
		return models.AccountData{}, err
	}

	return serverResponse.Data, nil
}
