FROM golang:1.16-alpine

ENV APP_SRC_DIR=/go/src
ENV GOPATH=/go

ENV CGO_ENABLED=0
ENV GOOS=linux
ENV GOARCH=amd64
ENV GOSUMDB=off

RUN mkdir -p $APP_SRC_DIR/app

WORKDIR $APP_SRC_DIR/app

ADD go.mod go.sum $APP_SRC_DIR/

RUN go mod download

ADD . $APP_SRC_DIR/app/

CMD ["go", "test", "-v", "./..."]
